<?php


function prArr($arr){
//    echo '<script>console.log('.json_encode($arr).')</script>';
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

// Заглавная буква в слове
function smi_fstoup($str){
    return mb_strtoupper(mb_substr($str,0,1)).mb_substr($str,1);
}

function smi_get_metabox_fields($table){
    $colName  = get_table_fields($table);
    if($colName) {
        $colName = array_fill_keys(array_values($colName), '');
        foreach ($colName as $key => $val) {
            $colName[$key] = isset($_POST[$key]) ? mb_strtolower(trim(preg_replace("/\s+?/", ' ', $_POST[$key]))) : '';
        }
    }
    return $colName;
}

function get_table_fields($table){
    global $wpdb;
    $query = 'SELECT * FROM '.$table;
    $wpdb->get_var($query);
    return $wpdb->get_col_info( 'name', -1 );
}


function getPeriod($str = 'w',$ind = 1){
    switch ($str){
        case 'y':
            $ind*= 12;
        case 'mm':
            $ind*= 4;
        case 'w':
            $ind*= 7;
        case 'd':
            $ind*= 24;
        case 'h':
            $ind*= 60;
        case 'm':
            $ind*= 60;
        case 's':
            $ind*= 1;
            break;
    }
    return  $ind;
}