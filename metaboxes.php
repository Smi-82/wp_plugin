<?php

add_action('add_meta_boxes','metaboxes_build');

function metaboxes_build(){
    global $wpdb;
    new SMI_Udo_Numb_Metabox($wpdb);
    new SMI_Personal_Data_Metabox($wpdb);
    new SMI_Factory_Data_Metabox($wpdb);
    new SMI_Errors_Data_Metabox($wpdb);
    new SMI_Certification_Types_Metabox($wpdb);
}

function forTermsSort($a,$b){
    if ($a->term_id == $b->term_id) {
        return 0;
    }
    return ($a->term_id < $b->term_id) ? -1 : 1;
};

function smi_valid_date($res,$str){
    $err = '';
    if($res) {
        $date = is_array($res)?$res[$str]:$res->$str;
        if (!empty($date)) {
            if (substr_count($date, '.')) {
                $err = 'class="errorDate"';
                $date.=' ошибка';
            } else
                $date = date("d.m.Y", intval($date));
        } else
            $date = '';
    }
    else
        $date = '';

    return array(
        'err'=>$err,
        'date'=>$date
    );

}
/**
 * Class Udo_Numb_Metabox
 */
class SMI_Udo_Numb_Metabox
{
    private $wpdb;
    /**
     * @param $id
     * @param $arr
     * @return bool
     */
    private function getrow( $id, $arr )
    {
        foreach($arr as $rez){
            if($rez->id_stand == $id)
                return $rez;
        }
        return false;
    }

    /**
     *
     */

    public function __construct(&$wpdb)
    {
        $this->wpdb = $wpdb;
        add_meta_box(
            'udo_numb_data',
            'Номера удостоверений',
            array($this,'build'),
            SMI_MY_TYPE,
            'normal',
            'high'
        );
    }

    /**
     *
     */
    public function build()
    {
        $post_id = get_the_ID();
        $terms = get_the_terms($post_id,'standarts');

        if($terms)
            usort($terms, 'forTermsSort');

        $query = 'SELECT * FROM '. SMI_UDO_NUMB.' WHERE id_men ='. $post_id;
        $res = $this->wpdb->get_results($query,OBJECT);
        $width = array(4,4,4);
        ?>
        <div id="table_udo_numb" class="data">
            <div class="row">
                <div class=" col-xs-<?php echo $width[0];?>">
                    <div class="cell">Стандарт</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <div class="cell">№ удостоверения</div>
                </div>
                <div class="col-xs-<?php echo $width[2];?>">
                    <div class="cell">Дата выдачи</div>
                </div>
            </div>
            <?php
            if($terms):
                $err = false;
                foreach($terms as $standart):?>
                    <?php
                    $term_id = $standart->term_id;
                    $row = $this->getrow( $term_id, $res );
                    $forDateArr = smi_valid_date($row, 'udo_date');
                    $numb =($row)?$row->udo_numb:'';
                    ?>
                    <div class="row">
                        <div class="col-xs-<?php echo $width[0];?>">
                            <div class="cell"><?php echo $standart->name;?></div>
                        </div>
                        <div class="col-xs-<?php echo $width[1];?>">
                            <input name="udo_numb-<?php echo $term_id;?>" type="text" value="<?php echo $numb; ?>">
                        </div>
                        <div class="col-xs-<?php echo $width[2];?>"<?php $err? print('class="errorInput"'):'';?>>
                            <input <?php echo $forDateArr['err']?> name="udo_date-<?php echo $term_id;?>" type="text" value="<?php echo $forDateArr['date'] ?>">
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
        <?php
    }
}


class SMI_Personal_Data_Metabox
{
    private $wpdb;

    public function __construct(&$wpdb)
    {
        $this->wpdb = $wpdb;
        add_meta_box(
            'personal_data',
            'Личные данные',
            array($this,'build'),
            SMI_MY_TYPE,
            'normal',
            'high'
        );
    }

    public function build()
    {
        $postID = get_the_ID();
        $query = 'SELECT * FROM '.SMI_PERSONAL_DATA." WHERE id_men = $postID";
        $res = $this->wpdb->get_row($query,OBJECT);
        $forDateArr = smi_valid_date($res, 'birthday');
        $width = array(3,9);
        ?>
        <div id="personal-data" class="data">
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Фамилия</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input type="text" name="sname" value="<?php echo $res?$res->sname:'';?>">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Имя</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>"><input type="text" name="fname" value="<?php echo $res?$res->fname:'';?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Отчество</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>"><input type="text" name="thname" value="<?php echo $res?$res->thname:'';?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Дата рождения</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>"><input <?php echo $forDateArr['err']?> type="text" name="birthday" value="<?php echo $forDateArr['date'];?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Телефоны</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>"><input type="text" name="phones" value="<?php echo $res?$res->phones:'';?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Адрес</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>"><textarea type="text" name="address" rows="5"><?php echo $res?$res->address:'';?></textarea></div>
            </div>
        </div>
        <?php
    }
}

class SMI_Factory_Data_Metabox
{
    private $wpdb;

    public function __construct(&$wpdb)
    {
        $this->wpdb = $wpdb;
        add_meta_box(
            'factory_data',
            'Заводские данные',
            array($this,'build'),
            SMI_MY_TYPE,
            'normal',
            'high'
        );
    }
    public function build()
    {
        $query = 'SELECT * FROM '.SMI_FACTORY_DATA.' WHERE id_men = '.get_the_ID();
        $res = $this->wpdb->get_row($query,OBJECT);
        $forExperDateArr = smi_valid_date($res, 'exper');
        $forTExperDateArr = smi_valid_date($res, 'total_exper');
        $width = array(3,9);
        ?>
        <div id="factory-data" class="data">
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Уникальный номер</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input type="text" name="un" value="<?php echo $res?$res->un:'';?>">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Дата поступления на завод</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input <?php echo $forExperDateArr['err'];?> type="text" name="exper" value="<?php echo $forExperDateArr['date'];?>">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Общий стаж с</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input <?php echo $forTExperDateArr['err'];?> ype="text" name="total_exper" value="<?php echo $forTExperDateArr['date'];?>">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">Клеймо сварщика</div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input type="text" name="mark" value="<?php echo $res?$res->mark:'';?>">
                </div>
            </div>
        </div>
        <?php
    }
}

class SMI_Errors_Data_Metabox
{
    private $wpdb;

    public function __construct(&$wpdb)
    {
        $this->wpdb = $wpdb;
        add_meta_box(
            'errors_data',
            'Замечания',
            array($this,'build'),
            SMI_MY_TYPE,
            'normal',
            'high'
        );
    }
    public function build()
    {
        $query = 'SELECT * FROM '.SMI_ERRORS_DATA.' ORDER BY err ASC';
        $forSelect = $this->wpdb->get_results($query,OBJECT );
        $t1 = SMI_ERRORS_MENS;
        $t2 = SMI_ERRORS_DATA;
        $post_id = get_the_ID();
        $query = "SELECT $t1.id, $t1.id_men, $t2.err, $t1.errdate, $t1.note FROM $t1 JOIN $t2 ON $t1.id_err = $t2.id_err AND id_men = $post_id ORDER BY 0+$t1.errdate DESC";
        $forRows = $this->wpdb->get_results($query,'OBJECT');
        $width = array(4,2,6);
        ?>
        <div id="errors_data" class="data">
            <div class="row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="cell">
                        Содержание несоответствия
                    </div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <div class="cell">Дата</div>
                </div>
                <div class="col-xs-<?php echo $width[2];?>">
                    <div class="cell">
                        Примечание
                    </div>
                </div>
            </div>
            <div class="row insert-row">
                <div class="col-xs-<?php echo $width[0];?>">
                    <div class="row select">
                        <div class="col-xs-10">
                            <input type="text" name="err" value="" id="errors_text" placeholder="содержание несоответствия">
                        </div>
                        <input type="hidden" name="id_err" value="0" id="errors-id">
                        <ul class="ulselect">
                            <?php foreach($forSelect as $err):?>
                            <li>
                                <div class="cell" id="<?php echo $err->id_err?>"><?php echo $err->err;?></div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                        <div class="col-xs-2 err_open_button">
                            <span class="toggle-indicator" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-<?php echo $width[1];?>">
                    <input type="text" name="errdate" value="" placeholder="Дата">
                </div>
                <div class="col-xs-<?php echo $width[2];?>">
                    <textarea type="text" name="note" placeholder="примечание"></textarea>
                </div>
                <div class="del_err_row">-</div>
            </div>
            <div class="err-row-container">
                <?php foreach($forRows as $err){
                    $forDateArr = smi_valid_date($err, 'errdate');
                    ?>
                    <div class="row" data="<?php echo $err->id;?>">
                        <div class="col-xs-<?php echo $width[0];?>">
                            <div class="cell"><?php echo $err->err;?></div>
                        </div>
                        <div class="col-xs-<?php echo $width[1];?>">
                            <input <?php echo $forDateArr['err'];?> type="text" name="for-err/errdate/<?php echo $err->id;?>/null" value="<?php echo $forDateArr['date'];?>">
                        </div>
                        <div class="col-xs-<?php echo $width[2];?>">
                            <textarea type="text" name="for-err/note/<?php echo $err->id;?>/null"><?php echo $err->note;?></textarea>
                            </div>
                        <div class="del_err_row">-</div>
                    </div>
                <?php }?>
            </div>
            <input type="hidden" name="trash-for-err" value="" id="trash-for-err">
        </div>
        <?php
    }
}

class SMI_Certification_Types_Metabox
{
    private $wpdb;
    private $forDateArr;
    private function prepareData(&$post_id)
    {
        $query = 'SELECT * FROM '.SMI_CERT_STAND_DATA.' WHERE id_men = '.$post_id.' ORDER BY 0+prot_date DESC';
        $result = $this->wpdb->get_results($query, OBJECT);
        $arr = array();

        if($result){

            foreach($result as $obj){
                if(!isset($arr[$obj->id_stand]))
                    $arr[$obj->id_stand] =array();
                if(!isset($arr[$obj->id_stand][$obj->id_cert])){
                    $arr[$obj->id_stand][$obj->id_cert] = array();
                }
                $arr[$obj->id_stand][$obj->id_cert][]=array(
                    'id' => $obj->id,
                    'numb_prot' => $obj->numb_prot,
                    'prot_date'=>$obj->prot_date,
                    'control_con'=> $obj->control_con,
                    'area'=>$obj->area
                );
            }
        }
        return $arr;
    }

    public function __construct(&$wpdb)
    {
        $this->wpdb = $wpdb;
        add_meta_box(
            'certification_data',
            'Виды аттестаций',
            array($this,'build'),
            SMI_MY_TYPE,
            'normal',
            'high'
        );
    }

    public function build()
    {
        $post_id = get_the_ID();
        $standarts = get_the_terms($post_id,'standarts');

        if($standarts)
            usort($standarts, 'forTermsSort');

        $certifications = get_the_terms($post_id,'certification');

        if($certifications)
            usort($certifications, 'forTermsSort');

        $data = $this->prepareData($post_id);
        $width = array(2,2,4,4);
        ?>
        <?php if($standarts && $certifications){?>
            <div id="standarts-table">
                <div class="row">
                    <?php foreach($standarts as $key=>$standart){?>
                        <div class="col-xs-<?php echo 12/count($standarts); ?> <?php echo $key==0?'activeSt':'';?>" id="standart-<?php echo $standart->term_id?>">
                            <div class="cell"><h3><?php echo $standart->name;?></h3></div>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div id="standarts-wrapper">
                <?php $cnt=0;?>
                <?php foreach($standarts as $standart){?>
                    <div class="certification-content" id="for-standart-<?php echo $standart->term_id?>">
                        <?php foreach($certifications as $cert){?>
                            <?php
                            $s_c = 'id_stand=>'.$standart->term_id.'/id_cert=>'.$cert->term_id;
                            ?>
                            <h3 class="cert_tables_title"><?php echo $cert->name?> аттестация по <?php echo $standart->name;?></h3>
                            <div class="cert_tables">
                                <div class="row">
                                    <div class="col-xs-<?php echo $width[0];?>">
                                        <div class="cell">Номер протокола</div>
                                    </div>
                                    <div class="col-xs-<?php echo $width[1];?>">
                                        <div class="cell">Дата</div>
                                    </div>
                                    <div class="col-xs-<?php echo $width[2];?>">
                                        <div class="cell">Контрольное соединение</div>
                                    </div>
                                    <div class="col-xs-<?php echo $width[3];?>">
                                        <div class="cell">Область распространения</div>
                                    </div>
                                </div>
                                <div class="row insert-row">
                                    <div class="col-xs-<?php echo $width[0];?>">
                                        <input type="text" name="<?php echo $cnt.'/'?>for_s_c/numb_prot/new/<?php echo $s_c;?>" value="" placeholder="номер протокола">
                                    </div>
                                    <div class="col-xs-<?php echo $width[1];?>">
                                        <input type="text" name="<?php echo $cnt.'/'?>for_s_c/prot_date/new/<?php echo $s_c;?>" value="" placeholder="дата">
                                    </div>
                                    <div class="col-xs-<?php echo $width[2];?>">
                                        <textarea name="<?php echo $cnt.'/'?>for_s_c/control_con/new/<?php echo $s_c;?>" cols="30" rows="2" placeholder="контрольное соединение"></textarea>
                                    </div>
                                    <div class="col-xs-<?php echo $width[3];?>">
                                        <textarea name="<?php echo $cnt.'/'?>for_s_c/area/new/<?php echo $s_c;?>" cols="30" rows="2" placeholder="область распростанения"></textarea>
                                    </div>
                                    <div class="delete-row">-</div>
                                </div>
                                <div class="rows-container">
                                    <?php
                                        $arr = $data[$standart->term_id][$cert->term_id];
                                    if(isset($arr)){
                                        foreach($arr as $val){
                                            $forDateArr = smi_valid_date($val, 'prot_date');
                                            ?>
                                            <div class="row" data="<?php echo $val['id'];?>">
                                                <div class="col-xs-<?php echo $width[0];?>">
                                                    <input type="text" name="for_s_c/numb_prot/old/<?php echo 'id=>'.$val['id'].'/null';?>" value="<?php echo $val['numb_prot'];?>">
                                                </div>
                                                <div class="col-xs-<?php echo $width[1];?>">
                                                    <input <?php echo $forDateArr['err'];?> type="text" name="for_s_c/prot_date/old/<?php echo 'id=>'.$val['id'].'/null';?>" value="<?php echo $forDateArr['date'];?>">
                                                </div>
                                                <div class="col-xs-<?php echo $width[2];?>">
                                                    <textarea name="for_s_c/control_con/old/<?php echo 'id=>'.$val['id'].'/null';?>" cols="30" rows="2"><?php echo $val['control_con'];?></textarea>
                                                </div>
                                                <div class="col-xs-<?php echo $width[3];?>">
                                                    <textarea name="for_s_c/area/old/<?php echo 'id=>'.$val['id'].'/null';?>" cols="30" rows="2"><?php echo $val['area'];?></textarea>
                                                </div>
                                                <div class="delete-row">-</div>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                            <hr>
                        <?php }?>
                        <?php $cnt++;?>
                    </div>
                <?php }?>
                <input type="hidden" name="trash-for-cert" value="" id="trash-for-cert">
            </div>
        <?php }?>
        <?php
    }
}
