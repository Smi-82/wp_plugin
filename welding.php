<?php

/*
Plugin Name: База сварщиков
Description: База сварщиков
Version: 1
Author: Сотников Михаил
*/

define('WELDING_PLUGIN_DIR',plugin_dir_path(__FILE__));


define('SMI_PERSONAL_DATA','smi_personal_data',TRUE);
define('SMI_FACTORY_DATA','smi_factory_data',TRUE);
define('SMI_ERRORS_DATA','smi_errors_data',TRUE);
define('SMI_UDO_NUMB','smi_udo_numb',TRUE);
define('SMI_ERRORS_MENS','smi_errors_mens',TRUE);
define('SMI_CLEANING','smi_cleaning',TRUE);
define('SMI_CERT_STAND_DATA','smi_cert_stand_data',TRUE);


define('SMI_MY_TYPE','welders',TRUE);


require_once(WELDING_PLUGIN_DIR . 'style-script.php');
require_once(WELDING_PLUGIN_DIR . 'activate.php');
require_once(WELDING_PLUGIN_DIR . 'deactivate.php');
require_once(WELDING_PLUGIN_DIR . 'db-manager.php');
require_once(WELDING_PLUGIN_DIR . 'metaboxes.php');
require_once(WELDING_PLUGIN_DIR . 'functions.php');

register_activation_hook(__FILE__,'activatePlugin');
register_deactivation_hook(__FILE__,'deactivatePlugin');

add_action( 'save_post', 'update_post' );
add_action( 'before_delete_post', 'deletePost' );
add_action( 'delete_term_taxonomy', 'deleteTerms', 10, 1);



function get_tables(){
    return array(
        SMI_PERSONAL_DATA,
        SMI_FACTORY_DATA,
        SMI_ERRORS_DATA,
        SMI_UDO_NUMB,
        SMI_ERRORS_MENS,
        SMI_CLEANING,
        SMI_CERT_STAND_DATA
    );
}


function smi_clean_DB(){
    global $wpdb;
    $query = 'SELECT cdate FROM '.SMI_CLEANING.' WHERE id = 1';

    $res = intval(array_shift($wpdb->get_row($query, ARRAY_N)));
    if((strtotime(date('d.m.Y'))-$res) > getPeriod()){
        $post_status = array(
            'auto-draft',
        );

        $post_status = implode(', ',array_map(function($m){
            return '\''.$m.'\'';
        },$post_status));
        $query = "SELECT ID FROM $wpdb->posts WHERE post_status IN ($post_status)";
        $res = $wpdb->get_results($query, ARRAY_N);

        if($wpdb->num_rows){
            $idArr = array();

            foreach($res as $id){
                $idArr[] = $id[0];
            }

            $idArr = implode(', ',$idArr);

            foreach(get_tables() as $table){
                $query = "DELETE FROM $table WHERE id_men IN ($idArr)";
                $wpdb->query($query);
            }

            $query = "DELETE FROM $wpdb->posts WHERE post_status IN ($post_status)";
            $wpdb->query($query);
        }
    }
}

function deleteTerms($termID){
    global $wpdb;
    $wpdb->delete(
        SMI_UDO_NUMB,
        array('id_cert'=>$termID),
        array( '%d' )
    );

}
function deletePost($postID){
    global $post_type;
    if ( $post_type != SMI_MY_TYPE )
        return;

    global $wpdb;

    $query = 'SELECT err_id FROM '.SMI_ERRORS_MENS.' WHERE id_men = '.$postID;

    $errId = $wpdb->get_results($query,ARRAY_N);

    if($wpdb->num_rows){
        $tmp = array();

        foreach($errId as $id){
            $tmp[] = $id[0];
        }

        $errId = implode(', ',$tmp);
        $query = 'DELETE FROM '.SMI_ERRORS_DATA." WHERE id IN ($errId)";
        $wpdb->query($query);
    }

    foreach(get_tables() as $table) {
        $wpdb->delete(
            $table,
            array('id_men' => $postID)
        );
    }
}


function update_post($post_id){

    if(get_post_type($post_id) == SMI_MY_TYPE) {

        $field = smi_get_metabox_fields(SMI_PERSONAL_DATA);
        $un = smi_get_metabox_fields(SMI_FACTORY_DATA);
        $un = empty($un['un'])?'':'-'.$un['un'];
        $title = smi_fstoup($field['sname']) . ' ' . smi_fstoup($field['fname']) . ' ' . smi_fstoup($field['thname']).$un;
        $arr = array('post_title' => $title, 'ID' => $post_id);
        if (!wp_is_post_revision($post_id)) {
            remove_action('save_post', 'update_post');
            wp_update_post($arr);
            add_action('save_post', 'update_post');
        }
        new Change_SMI_DB($post_id);
    }
    smi_clean_DB();
}







