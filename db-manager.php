<?php

class Change_SMI_DB
{
    private $wpdb;
    private $postID;

    private function dateValidate($date){
        $cntDot = substr_count($date,'.');
        if($cntDot > 2)
            return false;
        else if($cntDot == 1)
            $date.= '.'.date("Y");
        $arr = explode('.',$date);
        (strlen($arr[2]) == 1)?$arr[2]='200'.$arr[2]:'';
        if(strlen($arr[2]) == 2){
            if((2000+intval($arr[2])) > date("Y")){
                $arr[2] = 1900+intval($arr[2]);
            }
            else
                $arr[2] = 2000+intval($arr[2]);
        }
        elseif(strlen($arr[2]) == 0){
            $arr[2] = date("Y");
        }
        if(! checkdate( $arr[1], $arr[0], $arr[2])){
            return false;
        }
        return strtotime(implode('.',$arr));
    }
    private function getDate(&$field,$col)
    {
        if(!empty($field[$col])){
            $pattern = "~\D+~";
            $date = preg_replace($pattern, '.', $field[$col]);
            $date = $this->dateValidate($date);
            if($date == false){
                $date.= $field[$col].'.';
            }
        }
        else{
            $date = '';
        }
        return $date;
    }

    private function change_SMI_UDO_NUMB(){
        $terms = get_the_terms($this->postID,'standarts');
        $forreturn = array();
        $query = 'SELECT id_stand FROM '. SMI_UDO_NUMB." WHERE id_men = $this->postID";
        $res = $this->wpdb->get_results($query,OBJECT );
        if(!$terms || ($this->wpdb->num_rows > count($terms))){

            $tmpTerms = array();
            if($terms) {
                foreach ($terms as $term)
                    $tmpTerms[] = $term->term_id;
            }
            foreach($res as $tmp){
                if(empty($tmpTerms) || !in_array($tmp->id_stand,$tmpTerms)){
                    $forreturn[] = $tmp->id_stand;
                    $this->wpdb->delete( SMI_UDO_NUMB, array('id_men'=>$this->postID,'id_stand'=>$tmp->id_stand) );
                    $this->wpdb->delete( SMI_CERT_STAND_DATA, array('id_men'=>$this->postID,'id_stand'=>$tmp->id_stand) );
                }
            }
        }

        $numb="/^udo_numb-(\d+)/";
        $date="/^udo_date-(\d+)/";
        $arr = array();

        foreach ($_POST as  $key=>$val ) {
            $pattern = "~\D+~";
            $tmpKey = preg_replace($pattern,'',$key);
            if(preg_match ($numb,$key))
                $arr[$tmpKey]['udo_numb'] = trim($val);

            if(preg_match ($date,$key)) {
                $val = preg_replace($pattern, '.', $val);
                $arr[$tmpKey]['udo_date'] = trim($val);
            }
        }

        foreach ($arr as $key => $val) {
            if(!empty($val['udo_date'])) {
                $date = $this->dateValidate($val['udo_date']);
                if ($date == false) {
                    $arr[$key]['udo_date'] .= '.';
                } else {
                    $arr[$key]['udo_date'] = $date;
                }
            }
        }

        if($terms) {
            foreach ($terms as $term) {
                $term_id = $term->term_id;

                $query = 'SELECT * FROM '. SMI_UDO_NUMB." WHERE id_men = $this->postID AND id_stand = $term_id";
                $rez = $this->wpdb->get_row($query, OBJECT);

                if ($this->wpdb->num_rows) {
                    if (empty($arr[$term_id]['udo_numb']) || $rez->udo_numb != $arr[$term_id]['udo_numb']) {
                        $this->wpdb->update(
                            SMI_UDO_NUMB,
                            array('udo_numb' => $arr[$term_id]['udo_numb']),
                            array('id_men' => $this->postID, 'id_stand' => $term_id),
                            array('%s'),
                            array('%d', '%d')
                        );
                    }
                    if (empty($arr[$term_id]['udo_date']) || $rez->udo_date != $arr[$term_id]['udo_date']) {
                        $this->wpdb->update(
                            SMI_UDO_NUMB,
                            array('udo_date' => $arr[$term_id]['udo_date']),
                            array('id_men' => $this->postID, 'id_stand' => $term_id),
                            array('%s'),
                            array('%d', '%d')
                        );
                    }
                } else {

                    $this->wpdb->insert(SMI_UDO_NUMB,
                        array('id_men' => $this->postID, 'id_stand' => $term_id),
                        array('%d', '%d')
                    );
                }
            }
        }
        return $forreturn;
    }
    private function change_SMI_Personal_Data()
    {
        $table = SMI_PERSONAL_DATA;
        $field = smi_get_metabox_fields($table);
        $dateCol = 'birthday';
        $date = $this->getDate($field,$dateCol);
        $query = 'SELECT * FROM '.$table." WHERE id_men = $this->postID";
        $res = $this->wpdb->get_row($query, OBJECT );

        if($res){
            array_shift($field);
            $field[$dateCol] = $date;
            $this->wpdb->update(
                $table,
                $field,
                array('id_men'=>$this->postID)
            );
        }
        else{
            $field[$dateCol] = $date;
            $field['id_men'] = $this->postID;
            $this->wpdb->insert(
                $table,
                $field,
                array('%d', '%s', '%s', '%s', '%s', '%s', '%s')
            );
        }
    }

    private function change_SMI_Factory_Data()
    {
        $table = SMI_FACTORY_DATA;
        $field = smi_get_metabox_fields($table);
        $dateCol = 'exper';
        $Total_Exper = 'total_exper';
        $date = $this->getDate($field,$dateCol);
        $dateT_E = $this->getDate($field,$Total_Exper);
        $query = 'SELECT * FROM '.$table." WHERE id_men = $this->postID";
        $res = $this->wpdb->get_row($query, OBJECT );
        if($res){
            array_shift($field);
            $field[$dateCol] = $date;
            $field[$Total_Exper] = $dateT_E;
            $this->wpdb->update(
                $table,
                $field,
                array('id_men'=>$this->postID)
            );
        }
        else{
            $field[$dateCol] = $date;
            $field['id_men'] = $this->postID;
            $this->wpdb->insert(
                $table,
                $field,
                array('%d', '%s', '%s', '%s')
            );
        }
    }


    private function change_SMI_Errors_Data()
    {
        if(!empty($_POST['err'])) {
            // Проверка является ли несоответствие выбранным из списка или новое

            if ($_POST['id_err'] == 0) { // Если новое
                $field = smi_get_metabox_fields(SMI_ERRORS_DATA);
                array_shift($field);

                $this->wpdb->insert(
                    SMI_ERRORS_DATA,
                    $field,
                    array('%s')
                );

                $id = $this->wpdb->insert_id;
                $field = smi_get_metabox_fields(SMI_ERRORS_MENS);
                $field['id_err'] = $id;
            }
            else { // Если выбрано из списка
                $field = smi_get_metabox_fields(SMI_ERRORS_MENS);
            }

            // Запись в таблицу по конктретному сварщику
            $field['id_men'] = $this->postID;

            if (empty($field['errdate']))
                $field['errdate'] = strtotime(date('d.m.Y'));
            else
                $field['errdate'] = $this->getDate($field, 'errdate');

            array_shift($field);

            $this->wpdb->insert(
                SMI_ERRORS_MENS,
                $field,
                array('%d', '%d', '%s', '%s')
            );
        }

        // Поиск изменений в "Замечания"
        $newArr = array();
        foreach($_POST as $key=>$val){

            if(substr_count($key,'for-err') && substr_count($key,'change')){
                $tmp = explode('/',$key);
                if(!isset($newArr[$tmp[2]])){
                    $newArr[$tmp[2]] = array();
                }
                $newArr[$tmp[2]][$tmp[1]] = $val;
            }
        }

        if(!empty($newArr)){

            foreach($newArr as $key => $val){
                $val['errdate'] = $this->getDate($val, 'errdate');
                $this->wpdb->update(
                    SMI_ERRORS_MENS,
                    $val,
                    array( 'id' => $key, 'id_men'=>$this->postID ),
                    array( '%s', '%s' ),
                    array( '%d','%d')
                );
            }
        }

        // Удаление строк если необходимо

        if(!empty($_POST['trash-for-err'])){
            $query = 'DELETE FROM '.SMI_ERRORS_MENS." WHERE id IN ({$_POST['trash-for-err']}) AND id_men =".$this->postID;
            $this->wpdb->query($query);
        }
    }

    private function change_SMI_Cert_Stand_Data()
    {
        $newArr = array();
        $oldArr = array();
        foreach($_POST as $key => $val){
            if(substr_count($key,'for_s_c') && substr_count($key,'new') && !empty($val)){
                $newArr[$key] = $val;
            }
            elseif(substr_count($key,'for_s_c') && substr_count($key,'old') && substr_count($key,'change')){
                $oldArr[$key] = $val;
            }
        }
        if(!empty($newArr)){
            $arr = array();
            foreach($newArr as $key=>$val){

                $tmp = explode('/',$key);

                if(!isset($arr[$tmp[0]]))
                    $arr[$tmp[0]] = array();

                $tmp1 = explode('=>',$tmp[5]);

                if(!isset($arr[$tmp[0]][$tmp1[1]]))
                    $arr[$tmp[0]][$tmp1[1]] = array();

                $arr[$tmp[0]][$tmp1[1]][$tmp[2]] = $val;
                $tmp2 = explode('=>',$tmp[4]);
                $arr[$tmp[0]][$tmp1[1]][$tmp2[0]] = $tmp2[1];
                $arr[$tmp[0]][$tmp1[1]][$tmp1[0]] = $tmp1[1];
            }
            unset($newArr);

            foreach($arr as $queries){
                foreach($queries as $query){
                    $query['prot_date'] = empty($query['prot_date'])?strtotime(date('d.m.Y')):$this->getDate($query,'prot_date');
                    $query['id_men'] = $this->postID;
                    $this->wpdb->insert(
                        SMI_CERT_STAND_DATA,
                        $query
                    );
                }
            }
        }
        if(!empty($oldArr)){
            $arr = array();
            foreach($oldArr as $key=>$val) {

                $tmp = explode('/', $key);
                $tmp1 = explode('=>',$tmp[3]);
                if(!isset($arr[$tmp1[1]])){
                    $arr[$tmp1[1]] = array();
                }
                $arr[$tmp1[1]][$tmp[1]] = trim(preg_replace("~\s+?~",' ',$val));
            }
            foreach($arr as $key=>$query){
                if(isset($query['prot_date'])){
                    $query['prot_date'] = $this->getDate($query,'prot_date');
                }
                $this->wpdb->update(
                    SMI_CERT_STAND_DATA,
                    $query,
                    array(
                        'id'=>$key
                    ),
                    $format = null,
                    $where_format = null
                );
            }
        }
        if(!empty($_POST['trash-for-cert'])){
            $query = 'DELETE FROM '.SMI_CERT_STAND_DATA." WHERE id IN ({$_POST['trash-for-cert']}) AND id_men =".$this->postID;
            $this->wpdb->query($query);
        }

//        var_dump('sdf');
        $currentTerms = get_the_terms($this->postID,'certification');
        $allTerms = get_terms( 'certification', array('get' => 'all') );
        if(count($allTerms)>count($currentTerms)){
            $arrForDel = array();
            $tmp = array();
            if($currentTerms){
                foreach($currentTerms as $val){
                    $tmp[] = $val->term_id;
                }
                foreach($allTerms as $val){
                    if(!in_array($val->term_id, $tmp)){
                        $arrForDel[] = $val->term_id;
                    }
                }
                $str = implode(', ',$arrForDel);
                $query = 'DELETE FROM '.SMI_CERT_STAND_DATA." WHERE id_cert IN ($str) AND id_men=".$this->postID;
            }
            else{
                $query = 'DELETE FROM '.SMI_CERT_STAND_DATA.' WHERE id_men='.$this->postID;
            }
            $this->wpdb->query($query);
        }
    }
    public function __construct($postID)
    {
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->postID = $postID;

        $this->change_SMI_UDO_NUMB();
        $this->change_SMI_Personal_Data();
        $this->change_SMI_Factory_Data();
        $this->change_SMI_Errors_Data();
        $this->change_SMI_Cert_Stand_Data();
    }
    
}


