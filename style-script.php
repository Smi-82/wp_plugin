<?php
/**
 * Подключение стилей и скриптов плагина
 * ---------------------------------------------------------------------------------------------------------------------
 */
add_action( 'admin_enqueue_scripts', 'admin_scripts_connection' );




function admin_scripts_connection(){

    wp_register_style( 'adminStyle', plugins_url('welding/CSS/myadmin.css'));
    wp_register_script( 'adminScripts', plugins_url('welding/JS/admin.js'),array('jquery'));

    wp_enqueue_script('adminScripts');
    wp_enqueue_style( 'adminStyle' );
}