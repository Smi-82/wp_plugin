jQuery(document).ready(function($){

    $(window).resize(function(){
        makeRowsErrContainer();
        makeRowsContainer(1);
    });

    // Удаление строк из выбранной таблицы аттестации
    (function(){
        $('.delete-row').click(function(){
            var row = $(this).closest('.row');
            var val = '';
            var fl = true;
            if(row.hasClass('insert-row')){ // Удаление всех строк

                var parent = $(this).closest('.cert_tables');
                var rowCont = $('.rows-container',parent);
                ($('.row',rowCont).length == 0)?fl = false:'';

                $('.row',rowCont).each(function(){
                    var id = $(this).attr('data');
                    val = $('#trash-for-err').val();
                    val = val == ''?id:val+', '+id;
                    $('#trash-for-cert').val(val);
                    $(this).remove();
                });
            }
            else{ // Удаление конкретной строки
                var id = row.attr('data');
                val = $('#trash-for-cert').val();
                val = val == ''?id:val+', '+id;
                row.remove();
            }
            if(fl)
                $('#trash-for-cert').val(val);

            makeRowsContainer();
        });
    })();


    // Удаление строк из выбранной таблицы замечаний
    (function(){
        $('.del_err_row').click(function(){
            var row = $(this).closest('.row');
            var val = '';
            var fl = true;
            if(row.hasClass('insert-row')){ // Удаление всех строк

                var parent = $(this).closest('#errors_data');
                var rowCont = $('.err-row-container',parent);
                ($('.row',rowCont).length == 0)?fl = false:'';

                $('.row',rowCont).each(function(){
                    var id = $(this).attr('data');
                    val = $('#trash-for-err').val();
                    val = val == ''?id:val+', '+id;
                    $('#trash-for-err').val(val);
                    $(this).remove();
                });
            }
            else{ // Удаление конкретной строки
                var id = row.attr('data');
                val = $('#trash-for-err').val();
                val = val == ''?id:val+', '+id;
                row.remove();
            }
            if(fl)
                $('#trash-for-err').val(val);

            makeRowsErrContainer();
        });
    })();


    //Отображение блоков активной аттестации
    (function(){
        var activeSt = $('.activeSt');
        if(activeSt){
            var id = activeSt.attr('id');
            var cert_block = $('#for-'+id);
            if(cert_block)
                cert_block.addClass('certification-content-show');
        }
    })();

    // Установка ширины контейнера для строк в "Виды аттестации"
    makeRowsContainer(1);
    function makeRowsContainer(ind){
        $('.rows-container').each(function(){
            var parentWidth = $(this).closest('.cert_tables').width();
            $(this).width(parentWidth);
            var cntRow = $('.row',this).length;
            if(cntRow < 5)
                $(this).width(100+'%');
            if(ind != null) {
                containerWidth(this);
            }
        });
    }

    // Установка ширины контейнера для строк в "Замечания"
    makeRowsErrContainer();
    function makeRowsErrContainer(){
        var container = $('.err-row-container')[0];
        var parentWidth = $(container).closest('#errors_data').width();
        $(container).width(parentWidth);
        var cntRow = $('.row',container).length;
        if(cntRow < 5)
            $(container).width(100+'%');
        else {
            containerWidth(container);
        }
    }

    function containerWidth(tmp){
        var w = tmp.clientWidth;
        var offset = tmp.offsetWidth;
        if (offset > w) {
            $(tmp).width($(tmp).width() + (offset - w));
        }
    }

    // Контроль изменения данных в Таблице "Виды аттестаций"

    (function(){

        var tmp;

        $('.rows-container').on({'click':preChangeField, 'change':outField},'input, textarea');

        function outField(){
            var tmp1 = $(this).val();
            if(tmp1 != tmp){
                var name = $(this).attr('name').split('/');
                name[name.length-1] = 'change';
                name = name.join('/');
                $(this).attr('name',name);
            }
        }

        function preChangeField(){
            tmp = '';
            tmp = $(this).val();
        }

    })();

    // Контроль изменения данных в Таблице "Виды аттестаций"

    (function(){

        var tmp;

        $('.err-row-container').on({'click':preChangeField, 'change':outField},'input, textarea');

        function outField(){
            var tmp1 = $(this).val();
            if(tmp1 != tmp){
                var name = $(this).attr('name').split('/');
                name[name.length-1] = 'change';
                name = name.join('/');
                $(this).attr('name',name);
            }
        }

        function preChangeField(){
            tmp = '';
            tmp = $(this).val();
        }

    })();


    // Открытие блока выбора несоответствий
    $('.err_open_button').click(function(){
        $('.toggle-indicator',this).toggleClass('openErrButton');
        $('.ulselect','#errors_data').toggle('fast');
    });

    // Выбор несоответствия и подстановка его в input "Содержание несоответствия"
    $('.ulselect','.select').click(function(e){
        var elem = e.target;

        if($(elem).hasClass('cell')) {
            var input = $('#errors_text',$(elem).closest('.select'));
            $(input).val($(elem).html());
            $('#errors-id').val($(elem).attr('id'));
        }
    });

     // Закрытие блока выбора несоответствия при выходе курсора из его области
    $('.ulselect','.select').mouseleave(function(){

        $('.openErrButton').removeClass('openErrButton');
        $('.ulselect','#errors_data').hide('fast');

    });


    // Смена блоков видов аттестации под выбранный стандарт

    $('#standarts-table').click(function(e){
        //var elem = e.target;
        var parent = $(e.target).closest('[class*=col-]');
        if(!parent.hasClass('activeSt')){
            $('.activeSt',this).removeClass('activeSt');
            parent.addClass('activeSt');
            var id = parent.attr('id');
            var cert_block = $('#for-'+id);
            $('.certification-content-show').removeClass('certification-content-show');
            if(cert_block)
                cert_block.addClass('certification-content-show');
        }
    });

});