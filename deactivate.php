<?php

/**
 * Действия при деактивации плагина
 *
 */

function deactivatePlugin(){
    deleteDB();
}


function deleteDB(){
    $dbArray = array(
        SMI_PERSONAL_DATA,
        SMI_FACTORY_DATA,
        SMI_CERT_DATE,
        SMI_ERRORS_DATA,
        SMI_UDO_NUMB,
        SMI_ERRORS_MENS
    );

    global $wpdb;

    foreach($dbArray as $table){
        $query = 'DROP TABLE IF EXISTS '. $table;
        $wpdb->query($query);
    }
}