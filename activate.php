<?php

add_action('init','typeBuilder');
add_action('init','taxBuilder');



function typeBuilder(){
    welders_type();
}

function taxBuilder(){
    sectors_tax();
    rates_tax();
    standarts_tax();
    certification_tax();
}

/**
 * Создание собственных типов
 * ---------------------------------------------------------------------------------------------------------------------
 */


function welders_type(){
    $labels=array(
        'name'=>'Сварщики',
        'singular_name'=>'Сварщмк',
        'add_new'=>'Добавить сварщика',
        'add_new_item'=>'Добавить нового сварщика',
        'edit'=>'Редактировать',
        'edit_item'=>'Редактировать данные сварщика',
        'pfrent'=>'Родительская'
    );
    register_post_type(
        SMI_MY_TYPE,
        array(
            'labels'=>$labels,
            'public'=>true,
            'menu_position'=>0,
            'supports'=>array('title','thumbnail' ),
            'taxonomies'=>array('sectors','rates','standarts','certification'),
            'menu_icon'=>'',
            'has_archive'=>true
        )
    );
}

/**
 * Создание собственных таксономий
 * ---------------------------------------------------------------------------------------------------------------------
 */

function sectors_tax(){
    $labels=array(
        'name'=>'Цехи/Участки',
        'singular_name'=>'Цех/Участок'
    );
    register_taxonomy('sectors',SMI_MY_TYPE,array(
        'hierarchical'=>true,
        'labels'=>$labels,
        'show_ui'=>true,
        'show_admin_column'=>true,
        'query_var'=>true,

    ));
}

function rates_tax(){
    $labels=array(
        'name'=>'Разряды',
        'singular_name'=>'Разряд'
    );
    register_taxonomy('rates',SMI_MY_TYPE,array(
        'hierarchical'=>true,
        'labels'=>$labels,
        'show_ui'=>true,
        'show_admin_column'=>true,
        'query_var'=>true,

    ));
}

function standarts_tax(){
    $labels=array(
        'name'=>'Стандарты',
        'singular_name'=>'Стандарт'
    );
    register_taxonomy('standarts',SMI_MY_TYPE,array(
        'hierarchical'=>true,
        'labels'=>$labels,
        'show_ui'=>true,
        'show_admin_column'=>true,
        'query_var'=>true,

    ));
}

function certification_tax(){
    $labels=array(
        'name'=>'Аттестации',
        'singular_name'=>'Аттестация'
    );
    register_taxonomy('certification',SMI_MY_TYPE,array(
        'hierarchical'=>true,
        'labels'=>$labels,
        'show_ui'=>true,
        'show_admin_column'=>true,
        'query_var'=>true,

    ));
}

/**
 * Создание необходимых таблиц базы данных
 * ---------------------------------------------------------------------------------------------------------------------
 */

function activatePlugin(){
    new BuildTables();

}


class BuildTables
{
    private $wpdb;
    private $queryes;

    private function doQueryes()
    {
        foreach($this->queryes as $query)
            $this->wpdb->query($query);
    }
    private function setCleanTimer()
    {
        $query = 'SELECT cdate FROM '.SMI_CLEANING.' WHERE id = 1';
        $this->wpdb->get_row($query, OBJECT);

        if(!$this->wpdb->num_rows){
            $this->wpdb->insert(
                SMI_CLEANING,
                array('cdate'=>strtotime(date('d.m.Y'))),
                array('%s')
            );
        }

    }
    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;

        $this->queryes = array();

        $this->udo_numb();
        $this->personalData();
        $this->factoryData();
        $this->errorsData();
        $this->errorsMensData();
        $this->cleaning();
        $this->stand_cert();


        $this->doQueryes();

        $this->setCleanTimer();
    }

    /**
     *
     */
    private function udo_numb()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS '. SMI_UDO_NUMB.' (
                id_men INT,
                id_stand INT NOT NULL,
                udo_numb TEXT NOT NULL,
                udo_date TEXT NOT NULL
            )';

    }

    private function personalData()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_PERSONAL_DATA . ' (
                id_men INT,
                fname TEXT,
                sname TEXT NOT NULL,
                thname TEXT NOT NULL,
                birthday TEXT NOT NULL,
                phones TEXT NOT NULL,
                address TEXT NOT NULL
            )';
    }

    private function factoryData()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_FACTORY_DATA . ' (
                id_men INT,
                un TEXT NOT NULL,
                total_exper TEXT NOT NULL,
                exper TEXT NOT NULL,
                mark TEXT NOT NULL
            )';
    }
    private function errorsData()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_ERRORS_DATA . ' (
                id_err INT NOT NULL AUTO_INCREMENT ,
                err TEXT NOT NULL,
                PRIMARY KEY ( id_err )
            )';
    }
    private function errorsMensData()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_ERRORS_MENS . ' (
                id INT NOT NULL AUTO_INCREMENT,
                id_men INT,
                id_err INT NOT NULL,
                errdate TEXT NOT NULL,
                note TEXT,
                PRIMARY KEY ( id )
            )';
    }
    private function cleaning()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_CLEANING . ' (
                id INT NOT NULL AUTO_INCREMENT ,
                cdate TEXT NOT NULL,
                PRIMARY KEY ( id )
            )';
    }
    private function stand_cert()
    {
        $this->queryes[] = 'CREATE TABLE IF NOT EXISTS ' . SMI_CERT_STAND_DATA . ' (
                id INT NOT NULL AUTO_INCREMENT,
                id_men INT,
                id_stand INT,
                id_cert INT,
                numb_prot TEXT,
                prot_date TEXT,
                control_con TEXT,
                area TEXT,
                PRIMARY KEY ( id )
            )';
    }
}